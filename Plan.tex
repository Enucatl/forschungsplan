\section{Introduction and background}
Talbot-Lau interferometry is an imaging technique that allows simultaneous
access to phase and attenuation information of the X-rays.
It has been demonstrated on synchrotron sources since~2002~\cite{David2002,
Momose2003a, Weitkamp2005a} and on conventional sources with low spatial and
temporal coherence since 2006~\cite{Pfeiffer2006}. Its availability on
standard sources, such as those already used in hospitals and elsewhere for radiographic
and tomographic inspections, is of great interest.

The main advantages of phase-contrast imaging are an enhanced detection of
soft tissues with low absorption coefficients and a better discrimination of
different materials through complementary contrast mechanisms.
This can be particularly relevant for mammography~\cite{Hauser2013} or joint
diseases~\cite{Nagashima2013}, as well as nondestructive security or
material sciences investigations.

This work should further develop Talbot-Lau interferometry to a regime of
high photon energies, between~\SI{60}{\kilo\eV} and \SI{160}{\kilo\eV}.
These are indeed the typical energies for the operation of computed
tomography and airport security scanners, where higher energies are needed to
penetrate materials with large thicknesses or high atomic number.

Additionally, the dose delivery to the patients could be reduced through
phase sensitivity at high energies: the dose depends mainly on the
absorption and it goes down with the third power of the photon energy~\cite{Momose2005}. On
the other hand, the phase interaction is only inversely proportional to the
energy and is therefore relatively much more relevant than absorption as the
energy increases, possibly leading to a decreased exposure for an equivalent
image quality.

\section{Thesis aims and objectives}
The main aim of the project is a study of the feasibility and performance of
Talbot-Lau interferometry at high photon energies.

In recent years, other phase-sensitive techniques have been demonstrated on
sources with low spatial and temporal sources, such as coded
apertures~\cite{Munro2012} and analyzer-based
methods~\cite{Nesch2009,Parham2009}, but not yet at energies
abote~\SI{60}{\kilo\eV}.

Talbot-Lau interferometry has been until now limited by the fabrication
process of the gratings, since the ratio between the pitch and the depth of
the gratings, known as the aspect ratio, needs to increase with the design
energy of the interferometer, but is inherently limited in both
photolithography~\cite{David2002} and X-ray lithography~\cite{Mohr2012}.

A new arrangement of the gratings, introducing edge-on
illuminated structures~\cite{david2014method}, allows the realization of
Talbot-Lau interferometers at arbitrarily high design energies, by removing
this major technological hurdle, namely the fabrication of gratings with high
aspect ratios.

This research features both technical and scientific unknowns. From a
technical point of view, the challenge lies in the realization and operation
of one or more such interferometers with extremely tight precision and
stability requirements. This challenge is even more ambitious if we envisage
a deployment on a rotating, tomographic system.
Scientific questions are still open as well, most importantly about the
performance of this technique in a fundamentally different energy regime,
where Compton scattering becomes dominant with respect to photoelectric
absorption of X-rays. Can a Talbot-Lau interferometer work in this setting?
Is it still sensitive to complementary information about the sample?
What task would such a machine be best suited for?
Is it possible to find a compromise between image quality and delivered dose
that improves on the currently available radiographic or tomographic
systems?

These issues are expected to be addressed on both experimental and
theoretical grounds, thus bringing a significant contribution to the field of
X-ray imaging.

\section{Plan and publications}
At present, the following topics can be regarded as the most
promising for the development of the thesis. The submission of a manuscript
on each of these subjects, while still speculative at the moment, is envisaged.

\subsection{Design and realization of high-energy interferometers}
The design and demonstration of prototypes has been going on right since the
beginning of the doctoral project. The performance and reliability of these
prototypes will be assessed, with the ultimate goal of proving 
high-energy phase-contrast imaging for radiographic and,
if possible, on tomographic gantry systems.

It is important to note here that some technical challenges of this
experimental effort cannot at present be controlled from within the research
group. The most important of these is the fabrication of gratings with the
new edge-on design. The production of these optical elements is still
cutting edge technology, and is therefore very time consuming and error
prone.

First results from a prototype have been finalized as of the first quarter
of 2014,
and depending on the successful fabrication of more performing gratings, the
realization of more detailed studies with next generations of high-energy
interferometers can be envisaged in 2015 and 2016, particularly
for the investigation of gratings with larger pitches and longer setup distances.

\subsection{General feasibility and performance study}
The phase and absorption signals have a different energy dependence which,
as previously stated, makes phase-contrast imaging more interesting in a
high-energy setting.

However, the relationship between photon energy and different metrics for
image quality, such as resolution, signal-to-noise ratio, contrast-to-noise
ratio, etc., is a highly controversial topic in the X-ray imaging community
and needs to be consolidated on both experimental and theoretical grounds.

The current experiments cannot provide a reliable phase signal for
quantitative studies as of yet, but this can be solved through ongoing
projects in collaboration with the Karlsruhe Institute of Technology
as well as in-house developments.

\subsection{Clinical applicability and dose evaluation}

As a further development on the above section, the topic of image quality in
a clinical setting needs to be addressed. The most relevant and difficult
question concerns a quantification of the delivered dose again in connection
with image quality for a high-energy grating interferometer. The ultimate
goal would be the determination of optimum design parameters for specific
diagnostic tasks with an objective assessment of the improvements with
respect to a conventional absorption device.

This complex subject needs a multidisciplinary
approach, with contributions ranging from purely theoretical considerations
to simulations, from experimental measurements to comparisons with the most
recent regulations on dose deposition for diagnostic purposes. The
simulations on the other hand can be deployed as soon as possible, with
results coming during the spring and summer of 2014.

\subsection{Timeline}

The main milestones of the doctoral project can be summarized as follows:
\begin{enumerate}
    \item Construction and operation of a first generation of high-energy
        edge-on interferometers with design energies of~\num{60}, \num{100}
        and~\SI{120}{\kilo\eV}. A publication of these preliminary results
        was submitted at the end of 2013 and published in
        2014~\cite{Thuering2014b}.
    \item The development of a second generation of interferometers, with
        tuned parameters and a better fabrication quality is ongoing in
        2014, in collaboration with the Karlsruhe Institute of Technology. A
        new lab will be installed in the Swiss Light Source building
        in the second half of 2014. The gratings should be delivered in
        2015.
    \item Preliminary results suggest that the relationship between the
        three signals of grating interferometry changes significantly in the
        energy range above~\SI{80}{\kilo\eV}, where the Compton effect is
        dominant. Between the end of 2014 and the beginning of 2015 a
        description of this phenomenon should be completed, in particular
        concerning the relationship of the absorption and the dark field.
    \item With the development of such a theoretical framework, it
        will be possible to determine which clinically relevant applications
        can be realized. This should be the
        central result of the thesis, highlighting which areas of 
        medical radiography or material science are most likely to benefit
        from grating interferometry.
    \item Finally, with an improved set of gratings, it will be possible to
        include an experimental evaluation of the differential phase signal.
        Merging these experimental results with the theoretical description
        above and with the Monte Carlo simulations developed by the group
        would be a second main goal of this project. 
\end{enumerate}
\begin{figure}[h]
    \centering
    \includegraphics[width=\textheight, angle=90]{timeline.png}
\end{figure}
