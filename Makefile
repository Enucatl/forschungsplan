MAIN = forschungsplan
MAIN_TEX = $(MAIN).tex
MAIN_PDF = $(MAIN).pdf
DEPENDS = $(wildcard *.tex FrontBackmatter/*.tex Plan.tex)
BIBLIOGRAPHY = Bibliography.bib
FILE_CLEAN = *.aux *.bbl *.blg *.brf \
			 *.idx *.ilg *.ind *.log \
			 *.pdf *.lol *.lot *.lof \
			 *.tps *.tcp *.toc *.out \
			 FrontBackMatter/*.aux $(MAIN_PDF)

.PHONY: clean

$(MAIN_PDF): $(MAIN_TEX) $(BIBLIOGRAPHY) $(DEPENDS)
	pdflatex $(MAIN)
	bibtex $(MAIN)
	pdflatex $(MAIN)
	pdflatex $(MAIN)

clean:
	rm -f $(FILE_CLEAN) $(FILE_DISTCLEAN)
