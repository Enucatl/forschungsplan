example();

function example() {

var tasks = [
    {
    "startDate": new Date(12, 11, 1),
    "endDate": new Date(14, 06),
    "taskName": "First prototypes\n(60, 100 and 120 keV)",
    "status": "prototypes"
},
    {
    "startDate": new Date(13, 6, 1),
    "endDate": new Date(14, 2),
    "taskName": "Second generation\n(design)",
    "status":"second-design"
},
    {
    "startDate": new Date(14, 2, 1),
    "endDate": new Date(14, 12),
    "taskName": "Studies of dark field\nand absorption",
    "status":"df-abs"
},
    {
    "startDate": new Date(14, 12),
    "endDate": new Date(15, 12),
    "taskName": "Second generation\n(realization and experiments)",
    "status":"second-experiments"
},
    {
    "startDate": new Date(14, 12),
    "endDate": new Date(15, 8),
    "taskName": "Applications of high-energy\ngrating interferometry",
    "status":"applications"
},
    {
    "startDate": new Date(15, 8),
    "endDate": new Date(15, 12),
    "taskName": "Validation with phase data\nand simulations",
    "status":"validation"
},
    {
    "startDate": new Date(15, 12),
    "endDate": new Date(16, 3),
    "taskName": "Thesis",
    "status":"thesis"
},
];

var maxDate = tasks[tasks.length - 1].endDate;
var minDate = tasks[0].startDate;

var format = "%b %Y";

var taskStatus = {
    "prototypes": "prototypes",
    "second-design":"second-design",
    "second-experiments":"second-experiments",
    "df-abs":"df-abs",
    "applications":"applications",
    "validation":"validation",
    "thesis":"thesis"
    }

var taskTypes = tasks.map(function(d) {return d.taskName});

var gantt = d3.gantt()
    .taskStatus(taskStatus)
    .taskTypes(taskTypes)
    .tickFormat(format);

//gantt.timeDomain([new Date("Sun Dec 09 04:54:19 EST 2012"),new Date("Sun Jan 09 04:54:19 EST 2013")]);
//gantt.timeDomainMode("fixed");
gantt(tasks);


example.addTask = function() {
    
    var lastEndDate = Date.now();
    if (tasks.length > 0) {
	lastEndDate = tasks[tasks.length - 1].endDate;
    }
    
    var taskStatusKeys = Object.keys(taskStatus);
    var taskStatusName = taskStatusKeys[Math.floor(Math.random()*taskStatusKeys.length)];
    var taskName = taskNames[Math.floor(Math.random()*taskNames.length)];
    
    tasks.push({"startDate":d3.time.hour.offset(lastEndDate,Math.ceil(1*Math.random())),"endDate":d3.time.hour.offset(lastEndDate,(Math.ceil(Math.random()*3))+1),"taskName":taskName,"status":taskStatusName});
    gantt.redraw(tasks);
};

example.removeTask = function() {
    tasks.pop();
    gantt.redraw(tasks);
};

d3.select("#save").on("click", function(){
    var html = d3.select("svg")
    .attr("version", 1.1)
    .attr("xmlns", "http://www.w3.org/2000/svg")
    .node().parentNode.innerHTML;

    var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
    console.log(imgsrc);
    var img = '<img src="'+imgsrc+'">';
    d3.select("#svgdataurl").html(img);

    var canvas = document.querySelector("canvas"),
    context = canvas.getContext("2d");

    var image = new Image;
    image.src = imgsrc;
    image.onload = function() {
        context.drawImage(image, 0, 0);

        var canvasdata = canvas.toDataURL("image/png");

        var pngimg = '<img src="'+canvasdata+'">';
        d3.select("#pngdataurl").html(pngimg);

        var a = document.createElement("a");
        a.download = "sample.png";
        a.href = canvasdata;
        a.click();
    };

});

};
